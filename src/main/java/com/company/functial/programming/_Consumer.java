package com.company.functial.programming;

import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        sayHello("Ann");

        Consumer<String> consumer = (name) -> System.out.println("Hello " + name);
        consumer.accept("Lilit");
    }
    static void sayHello(String name) {
        System.out.println("Hello " + name);
    }
}
