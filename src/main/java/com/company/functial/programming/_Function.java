package com.company.functial.programming;

import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {
        int result = incrementByOne(1);
        System.out.println(result);

        Function<Integer, Integer> incrementByOneFunction = (number) -> number + 1;
        int result2 = incrementByOneFunction.apply(1);
        System.out.println(result2);

        Function<Integer, Integer> multiplyByTwoFunction = (number) -> number * 2;
        int result3 = multiplyByTwoFunction.apply(result2);
        System.out.println(result3);

        Function<Integer, Integer> combinationFunction = incrementByOneFunction.andThen(multiplyByTwoFunction);
        int result4 = combinationFunction.apply(1);
        System.out.println(result4);

    }
    static int incrementByOne(int number) {
        return number + 1;
    }
}
