package com.company.functial.programming;

import java.util.function.Predicate;

public class _Predicate {
    public static void main(String[] args) {
        System.out.println(isAgeValid(1));
        System.out.println(isAgeValid(-1));

        Predicate<Integer> predicate = (age) -> age > 0;
        System.out.println(predicate.test(1));

        Predicate<Integer> adultPredicate = (age) -> age >= 18;
        System.out.println(predicate.test(19));

        Predicate<Integer> combinationPredicate = predicate.and(adultPredicate);
        System.out.println(combinationPredicate.test(19));
    }
    static boolean isAgeValid(int age) {
        return age > 0;
    }
}
