package com.company.functial.programming;

import java.util.function.Supplier;

public class _Supplier {
    public static void main(String[] args) {
        System.out.println(getMyEmail());

        Supplier<String> supplier = () -> "lilittovmasyan999@gmail.com";
        System.out.println(supplier.get());
    }
    static String getMyEmail() {
        return "lilittovmasyan999@gmail.com";
    }
}
