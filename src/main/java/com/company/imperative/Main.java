package com.company.imperative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User("Sam", 20),
                new User("Anna", 15),
                new User("John", 23),
                new User("Poul", 17)
        );

        // Imperative approach

        System.out.println("Imperative approach");
        // Get all users >= 18
        List<User> adultUsers = new ArrayList<>();
        for (User user: users) {
            if (user.getAge() >= 18) {
                adultUsers.add(user);
            }
        }

        // Print them
        for (User user: adultUsers) {
            System.out.println(user);
        }

        // Declarative approach

        System.out.println("Declarative approach");
        users.stream()
                .filter(user -> user.getAge() >= 18)
                .collect(Collectors.toList())
                .forEach(System.out::println);

    }
}
