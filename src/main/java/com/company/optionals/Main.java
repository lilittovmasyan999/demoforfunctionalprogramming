package com.company.optionals;

import java.util.Optional;

public class Main {
    String name;
    public static void main(String[] args){
        Main m = new Main();
//        char ch = m.name.charAt(0);
//        System.out.println(ch);

        Optional<String> isNull = Optional.ofNullable(m.name);

//        isPresent() method returns true if the variable is not null
        if(isNull.isPresent()) {
            char ch = m.name.charAt(0);
            System.out.println(ch);
        } else {
            System.out.println("Value is null");
        }

//        ifPresent() method executes only if the variable is not null, else it does nothing.
        isNull.ifPresent(n -> {
                    System.out.println("Value is present");
                    System.out.println("Value start with "+ m.name.charAt(0));
                }
        );


        Optional<String> name = Optional.of("Rohan");
        String other = name.orElse("Sohan");
        System.out.println(other);
    }
}
