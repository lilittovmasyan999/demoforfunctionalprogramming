package com.company.streams;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;
import java.util.List;

public class MethodReference {
    public static void main(String[] args) {

        List<String> names = Arrays.asList("ann", "bob", "john");

        // 1st capitalize names
        // 2nd print hello message to everyone using hello() method

        names.stream()
                .map(name -> StringUtils.capitalize(name))
                .forEach(name -> hello(name));

        names.stream()
                .map(StringUtils:: capitalize)
                .forEach(MethodReference::hello);

    }

    static void hello(String name) {
        System.out.println("Hello " + name);
    }
}
