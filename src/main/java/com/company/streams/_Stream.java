package com.company.streams;

import com.company.imperative.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class _Stream {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User("Sam", 20),
                new User("Anna", 15),
                new User("John", 23),
                new User("Poul", 17)
        );

       users.stream()
                .filter(user -> user.getAge() >= 18)
                .collect(Collectors.toList())
                .forEach(user -> System.out.println(user));

       users.stream()
               .mapToInt(user -> user.getName().length())
               .forEach(nameLength -> System.out.println(nameLength));

       users.stream()
               .map(user -> user.getAge() * 2)
               .forEach(nameLength -> System.out.println(nameLength));

        boolean result = users.stream()
                .allMatch(user -> user.getAge() >= 18);

        System.out.println(result);

    }
}
